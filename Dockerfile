# aicacia/build-agent-node:${NODE_VERSION}

FROM aicacia/docker-kube-helm:19.03-1.17-3.1.2

RUN apt update \
  && apt install -y \
    apt-transport-https \
    build-essential \
    ca-certificates \
    curl \
    git \
    libssl-dev

ENV NVM_DIR /usr/local/nvm
RUN mkdir -p /usr/local/nvm

SHELL [ "/bin/bash", "-l", "-c" ]

ARG NODE_VERSION=13.12.0
ENV NODE_VERSION_LOCAL $NODE_VERSION
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash