#!/bin/bash

repo="aicacia/build-agent-node"
set -e

function build_and_push() {
  local version=$1
  local full_version=$2
  
  docker build --build-arg NODE_VERSION=${full_version} -t ${repo}:${full_version} .
  docker push ${repo}:${full_version}

  docker tag ${repo}:${full_version} ${repo}:${version}
  docker push ${repo}:${version}

  docker tag ${repo}:${version} ${repo}:latest
  docker push ${repo}:latest
}

build_and_push "8" "8.17.0"
build_and_push "9" "9.11.2"
build_and_push "10" "10.19.0"
build_and_push "11" "11.15.0"
build_and_push "12" "12.16.1"
build_and_push "13" "13.12.0"